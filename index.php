<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>S05: Activity</title>
    </head>

    <body>
        
        <?php session_start(); ?>

        <?php if (!isset($_SESSION['credentials'])): ?>

            <form method="POST" action="./server.php" value="login">
                <h1>Login</h1>
                <input type="hidden" name="action" value="login">
                Email:
                <input type="text" name="username" required>
                Password:
                <input type="text" name="password" required>
                <input type="submit" value="Login">
            </form>
        
        <?php endif; ?>

        <?php if (isset($_SESSION['credentials'])): ?>

            <form method="POST" action="./server.php" value="logout">
                <h1>Welcome</h1>
                <input type="hidden" name="action" value="logout">
                <p>Hello, johnsmith@gmail.com</p>
                <input type="submit" value="Logout">
            </form>

        <?php endif; ?>

    
    </body>
</html>